/**
 * User.js
 *
 * @description :: Model for managing logging in and creating new users (signing up)
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');
var salt = bcrypt.genSaltSync(10);

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true,
      minLength: 3,
      maxLength: 20
    },
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      required: true,
    }
  },

  signup: function (inputs, cb) {
    User.create({
      name: inputs.name,
      email: inputs.email,
      password: bcrypt.hashSync(inputs.password, salt)
    })
    .exec(cb);
  },

  login: function (inputs, cb) {
    User.findOne({
      email: inputs.email
    }).exec(function(err, user) {
      if(user) {
        if(bcrypt.compareSync(inputs.password, user.password)) {
          cb(null, user)
        } else {
          cb("invalid username/password")
        }
      } else {
        cb("invalid username/password")
      }
    })
  }
};

