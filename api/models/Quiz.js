/**
 * Quiz.js
 *
 * @description :: Model for managing quizzes
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    category: {
      type: 'string',
      required: true
    },
    taker: {
      model: 'user'
    },
    questions: {
      collection: 'question',
      via: 'quizzes',
      dominant: true
    },
    currentQuestion: {
      model: 'question'
    }
  },
  
  generateQuiz: function (inputs, cb) {
    Quiz.create({
      category: inputs.category
    }).exec(function(err, quiz) {
      Quiz.find({id: quiz.id}).populate('questions').exec(function (err, res) {
        Question.find({category: inputs.category}).exec(function (err, res2) {
          res2.forEach(function(stuff) {
            res[0].questions.add(stuff.id)
          })
          res[0].save(function() {
            Quiz.find({id: quiz.id}).exec(cb)
          })
        })
      })
    })
  }
};

