/**
 * Answer.js
 *
 * @description :: Model for managing answers
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    question: {
      model: 'question'
    },
    text: {
      type: 'string',
      required: true
    },
    quiz: {
      model: 'quiz'
    }
  }
};

