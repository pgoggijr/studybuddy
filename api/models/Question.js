/**
 * Question.js
 *
 * @description :: Model for managing a single question
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    text: {
      type: 'string',
      required: true
    },
    category: {
      type: 'string',
      required: true
    },
    quizzes: {
      collection: 'quiz',
      via: 'questions'
    },
    answers: {
      collection: 'answer',
      via: 'quiz'
    },
    correctAnswer: {
      model: 'answer'
    },
    attemptedAnswer: {
      model: 'answer'
    }
  },

  createQuestion: function (inputs, cb) {
    Question.create({
      text: inputs.text,
      category: inputs.category,
    }).exec(function (err, question) {
      Question.find({id: question.id}).populate('answers').exec(function (err, res) {
        res[0].answers.add({text: inputs.wrong1})
        res[0].answers.add({text: inputs.wrong2})
        res[0].answers.add({text: inputs.wrong3})
        res[0].answers.add({text: inputs.correct})
        res[0].save(function(err, res2) {
          Question.find(question.id).populate('answers').exec(function (err, res2) {
            Question.update(question.id, { correctAnswer: res2[0].answers[3].id}).exec(function() {
              Question.find(question.id).populate('answers').populate('correctAnswer').exec(cb)
            })
          })
        })
      })
    })
  }
}

