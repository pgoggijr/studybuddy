/**
 * QuizController
 *
 * @description :: Server-side logic for managing quizzes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  generateQuiz: function (req, res) {
    Quiz.generateQuiz({
      category: req.param('category')
    })
  }
};

