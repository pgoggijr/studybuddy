/**
 * UserController
 *
 * @description :: Server-side logic for managing user logins/logouts/signups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var jwt = require('jsonwebtoken');

module.exports = {

  /**
   * `UserController.login()`
   */
  login: function (req, res) {
    User.login({
      email: req.param('email'),
      password: req.param('password'),
    }, function (err, user) {
      if (err) {
        return res.negotiate(err);
      }
      if (!user) {
        if (req.wantsJSON || !inputs.invalidRedirect) {
          return res.badRequest('Invalid username/password');
        } else {
          console.log("rendering main")
          return res.view('main');
        }
      } else {
        var token = jwt.sign({user: user.id}, sails.config.jwtSecret, {expiresIn: sails.config.jwtExpires});
        console.log("rendering main")
        //return res.redirect('assets/public/main.html');
        return res.redirect('public/main.html');
        return res.view('main', {});
      }
    });
  },


  /**
   * `UserController.logout()`
   */
  logout: function (req, res) {
    req.session.me = null;
    if (req.wantsJSON) {
      return res.ok('Logged out');;
    } else {
      return res.redirect('/');
    }
  },

  /**
   * `UserController.signup()`
   */
  signup: function (req, res) {
    User.signup({
      name: req.param('name'),
      email: req.param('email'),
      password: req.param('password')
    }, function(err, user) {
      if (err) return res.negotiate(err);

      var token = jwt.sign({user: user.id}, sails.config.jwtSecret, {expiresIn: sails.config.jwtExpires});

      if(req.wantsJSON) {
        return res.ok(token);
      } else {
        return res.view('main')
      }
    })
  }
};

