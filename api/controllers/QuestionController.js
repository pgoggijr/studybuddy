/**
 * QuestionController
 *
 * @description :: Server-side logic for managing questions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  createQuestion: function (req, res) {
    console.log("swag")
    Question.createQuestion({
      text: req.param('text'),
      category: req.param('category'),
      wrong1: req.param('wrong1'),
      wrong2: req.param('wrong2'),
      wrong3: req.param('wrong3'),
      correct: req.param('correct')
    }, function(err, question) {
      res.ok(question)
    })
  }
};

